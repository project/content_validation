<?php

namespace Drupal\content_validation\Plugin\Validation\Constraint;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Implementing Content Validation Class.
 */
class ContentValidationConstraintsValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Creates a new ContentValidationConstraintsValidator instance.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack.
   */
  public function __construct(ConfigFactoryInterface $config_factory, RequestStack $requestStack) {
    $this->configFactory = $config_factory;
    $this->requestStack = $requestStack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function validate($value, Constraint $constraint) {
    if ($value instanceof FieldItemListInterface) {
      $string_to_test = $value->getValue();
      if (!empty($string_to_test[0]['value'])) {
        // Get the regex set.
        $regexes = $this->configFactory->get('content_validation.settings')->get('regex');
        if (!empty($regexes)) {
          $regexDecoded = Json::decode($regexes);
          // Check if the content validation is not disabled.
          $content_validation_disabled = $this->requestStack->getCurrentRequest()->cookies->get('content_validation');
          if (empty($content_validation_disabled)) {
            foreach ($regexDecoded as $regex) {
              // Check values againts regex.
              if (preg_match($regex, $string_to_test[0]['value'], $matches)) {
                $this->context->addViolation($constraint->contentValidation, ['%word' => $matches[0]]);
              }
            }
          }
        }
      }
    }
  }

}
