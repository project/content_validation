<?php

namespace Drupal\content_validation\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks that the submitted value does not contains forbidden words.
 *
 * @Constraint(
 *   id = "ContentValidationConstraints",
 *   label = @Translation("Content validation", context = "Validation"),
 * )
 */
class ContentValidationConstraints extends Constraint {

  /**
   * Message when the field content contains forbidden words.
   *
   * @var string
   */
  public $contentValidation = 'This field contains forbidden words: %word';

}
