<?php

namespace Drupal\content_validation\Form;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form that configures Content Validation settings.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'content_validation_admin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'content_validation.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL) {
    $config = $this->config('content_validation.settings');
    $enable_content_validation = $config->get('enable_content_validation');
    $form['enable_content_validation'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Content Validation'),
      '#description' => $this->t('This box will enable and disable content validation functionality.'),
      '#default_value' => isset($enable_content_validation) ? $enable_content_validation : TRUE,
    ];

    $form['patterns'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Content validation regex'),
      '#description' => $this->t('Enter forbidden words in multiple lines. Wildcards (*) are supported and matching is case-insensitive.'),
      '#default_value' => $config->get('patterns'),
      '#rows' => 20,
    ];

    $form['regex'] = [
      '#type' => 'hidden',
      '#value' => $config->get('regex'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $regex_strings = [];
    $patterns = preg_split('/\r\n|\r|\n/', $form_state->getValue('patterns'));
    foreach ($patterns as $key => $pattern) {
      $pattern = trim($pattern, " \t\n\r\0\x0B\*");
      // Replace spaces with with regex.
      $pattern = preg_replace('/\s+/', '\s+', $pattern);
      $pattern = str_replace('*', '(.)*', $pattern);

      if (!empty($pattern)) {
        if (@preg_match('/' . $pattern . '/i', NULL) === FALSE) {
          $form_state->setErrorByName('regex', $this->t('The regex from line @key is invalid.', ['@key' => $key + 1]));
        }
        else {
          $regex_strings[] = '/' . $pattern . '/i';
        }
      }
    }

    if (!empty($regex_strings)) {
      $form_state->setValue('regex', Json::encode($regex_strings));
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('content_validation.settings')
      ->set('enable_content_validation', $form_state->getValue('enable_content_validation'))
      ->set('patterns', $form_state->getValue('patterns'))
      ->set('regex', $form_state->getValue('regex'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
