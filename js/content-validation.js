(function ($, Drupal) {

  "use strict"

  Drupal.behaviors.contentValidation = {
    attach: function (context, settings) {
      $("input[name='content_validation']").change(function() {
        if (this.checked) {
          $.cookie('content_validation', 1, {
            domain: '',
            path: '/'
          });
        }
        else {
          $.removeCookie('content_validation',  { path: '/' });
        }
      });
    }
  }

})(jQuery, window.Drupal);

